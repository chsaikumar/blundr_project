/**
 * Creating the main class to perform user operations
 * @author schinthakunta1
 *
 */
public class Main {

	public static void main(String[] args) {
		AllUsers user=new AllUsers();
		
		user.createNewBlundrUser("Sai","20", "I am software engineer", "Coding");
		user.createNewBlundrUser("Ashok","25", "I am software developer", "developing");
		user.createNewBlundrUser("Anil","21", "I am software engineer", "testing");
	}

}
